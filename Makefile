VERSION=$(shell cat VERSION)

DOCKER_NAME = registry.gitlab.com/lisael/santa/python3
TEST_PROJ_DOCKER_NAME = registry.gitlab.com/lisael/santa/testproj
SUDO?=sudo
WWW_BROWSER?=firefox

PYTHON=/usr/bin/python3

TESTPROJ?=testproj
TESTPROJ_ENV=$(TESTPROJ)/venv
TESTPROJ_BIN=$(shell readlink -e $(TESTPROJ_ENV)/bin)
TESTPROJ_PYTHON=$(TESTPROJ_BIN)/python
TESTPROJ_PIP=$(TESTPROJ_ENV)/bin/pip

docker_build_dev:
	cp ./dev_requirements.txt docker/pythontest/dev_requirements.txt
	$(SUDO) docker build -t $(DOCKER_NAME):latest docker/pythontest/
	rm docker/pythontest/dev_requirements.txt

docker_push_dev: docker_build_dev
	$(SUDO) docker push $(DOCKER_NAME)

docker_build_testproj: testproj_destroy
	rm -rf docker/testproj/testproj
	cp -r $(TESTPROJ) docker/testproj
	$(SUDO) docker build -t $(TEST_PROJ_DOCKER_NAME):latest docker/testproj
	rm -rf docker/testproj/testproj

docker_push_testproj: docker_build_testproj
	$(SUDO) docker push $(TEST_PROJ_DOCKER_NAME)

docker_run_testporj:
	$(SUDO) docker run -p 127.0.0.1:8080:8080 $(TEST_PROJ_DOCKER_NAME)

coverage:
	pytest --cov=santa --cov-report=html --cov-report=term -vv -s --full-trace tests

api_coverage: 
	cd $(TESTPROJ); $(TESTPROJ_BIN)/coverage run --source=tutorial,snippets ./manage.py runserver --noreload 0.0.0.0:8080
	cd $(TESTPROJ); $(TESTPROJ_BIN)/coverage html
	$(WWW_BROWSER) $(TESTPROJ)/htmlcov/index.html

$(TESTPROJ_ENV):
	virtualenv --python=$(PYTHON) $(TESTPROJ_ENV)

testproj_venv: $(TESTPROJ_ENV) $(TESTPROJ)/requirements.txt
	$(TESTPROJ_PIP) install -U pip
	$(TESTPROJ_PIP) install -r $(TESTPROJ)/requirements.txt
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) ./manage.py migrate

testproj_run: testproj_venv testproj_restore
	cd $(TESTPROJ); $(TESTPROJ_BIN)/gunicorn tutorial.wsgi -b 0.0.0.0:8080 --log-level debug --log-file -

testproj_runserver: testproj_venv testproj_restore
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) manage.py runserver 0.0.0.0:8080

testproj_restore:
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) manage.py flush --noinput
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) manage.py loaddata snippets/fixtures/users.json
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) manage.py loaddata snippets/fixtures/snippets.json

testproj_shell: testproj_venv
	cd $(TESTPROJ); $(TESTPROJ_PYTHON) manage.py shell_plus

testproj_destroy:
	rm -rf $(TESTPROJ_ENV)
	rm -f $(TESTPROJ)/db.sqlite3

clean:
	find ./santa -type d -name __pycache__ -exec echo "rm -rf {}" \;
	rm -rf build
	rm -rf dist

build:
	./setup.py sdist bdist_wheel 

upload:
	twine upload --repository-url https://upload.pypi.org/legacy/ dist/*

bump:
	git commit -am "Bump to version $(VERSION)"
	git tag $(VERSION)
	git push
	git push --tags

release: clean bump build upload


.PHONY: docker_build_dev docker_push_dev coverage docker_build_testproj docker_push_testproj docker_run_testporj testproj_restore testproj_destroy testproj_runserver clean build upload bump release

