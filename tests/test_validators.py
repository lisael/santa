import pytest

from santa.validator import Validator, ValidationError, validator_factory
from santa.builtins.validators import StatusValidator, Json


@pytest.mark.asyncio
async def test_validator():
    class MyValidator(Validator):
        __yaml_name__ = "myvalidator"
        async def __call__(self, data, ctx):
            return
    v = validator_factory({"myvalidator": {}})
    await v(1, 2)


class MockContext:
    def render(self, msg):
        return msg


class MockResp:
    def __init__(self, status, json=None):
        self.status = status
        self._json = json

    async def json(self):
        return self._json


@pytest.mark.asyncio
async def test_status_validator():
    v = validator_factory(dict(status=[1, 2, 3]))
    # v = StatusValidator(items=[1, 2, 3])
    await v(MockResp(1), None)
    with pytest.raises(ValidationError):
        await v(MockResp(4), None)


@pytest.mark.asyncio
async def test_json_validator():
    v = Json(pattern=".foo", equals="bar")
    await v(MockResp(200, json={"foo": "bar"}), MockContext())
    with pytest.raises(ValidationError):
        await v(MockResp(200, json={"foo": "baz"}), MockContext())

    v = Json(partial=dict(foo=42, bar=dict(baz=12)))
    await v(MockResp(200, json={"foo": 42, "bar":{"baz": 12}}), MockContext())
    with pytest.raises(ValidationError):
        await v(MockResp(200, json={"foo": "baz"}), MockContext())
    with pytest.raises(ValidationError):
        await v(MockResp(200, json={"bar": "baz"}), MockContext())

    v = Json(contains=["foo"])
    await v(MockResp(200, json={"foo": 42, "bar":{"baz": 12}}), MockContext())
    with pytest.raises(ValidationError):
        await v(MockResp(200, json={"bar": "baz"}), MockContext())

    v = Json(partial_match=dict(foo="ba."))
    await v(MockResp(200, json={"foo": "bar"}), MockContext())
    await v(MockResp(200, json={"foo": "baz"}), MockContext())
    with pytest.raises(ValidationError):
        await v(MockResp(200, json={"foo": "fubar"}), MockContext())

