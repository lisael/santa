import os
import pytest
from santa.context import context_from_file, UndefinedError, mergedicts, Context


HERE = os.path.dirname(__file__)


@pytest.fixture
def ctx():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    return ctx


def test_create():
    ctx = Context()
    assert len(ctx.keys()) == 0


def test_load(ctx):
    assert ctx['url']['base_url'] == "http://example.com"
    assert ctx['url']['url'] == "http://example.com/stuff"


def test_template(ctx):
    with pytest.raises(UndefinedError):
        ctx.render("{{ user.staff.login }}")
    data = "{{ guess.what? }}"
    assert ctx.render(data) == data


def test_copy(ctx):
    cpy = ctx.copy()
    assert ctx["user"]["staff"]["password"] == cpy["user"]["staff"]["password"]

    cpy["user"]["staff"]["password"] = "other"
    assert ctx["user"]["staff"]["password"] != cpy["user"]["staff"]["password"]


def test_udpate(ctx):
    # before applying overlay
    assert ctx['url']['scheme'] == "http"
    assert ctx['url']['query_string']['bar'] == "bar"
    with pytest.raises(KeyError):
        ctx['url']['foo']
    with pytest.raises(KeyError):
        ctx['url']['query_string']['fubar']

    # apply
    overlay = dict(
            url=dict(
                scheme="https",
                foo="bar",
                query_string=dict(
                    bar="baz",
                    fubar="fubar")))
    ctx.update(overlay)

    # after apply
    # change append
    assert ctx['url']['scheme'] == "https"
    # additionnal stuff was added
    assert ctx['url']['foo'] == "bar"
    # same for nested dicts
    assert ctx['url']['query_string']['bar'] == "baz"
    assert ctx['url']['query_string']['fubar'] == "fubar"


def test_undefined(ctx):
    with pytest.raises(UndefinedError) as e:
        ctx['user']['staff']['login']
    assert 'is not defined' in str(e.value)
    ctx['user']['staff']['login'] = "staff user"
    assert ctx['user']['staff']['login'] == "staff user"


def test_mergedicts():
    dest = {"a": 1, "b": 2, "nested": {"d": 4}}
    src = {"a": 4, "c": 3, "nested": {"d": 5}}
    assert mergedicts(dest, src) == {"a": 4, "b": 2, "c": 3,
                                     "nested": {"d": 5}}
    assert dest == {"a": 4, "b": 2, "c": 3,
                    "nested": {"d": 5}}


def test_overlay(ctx):
    with open(os.path.sep.join([HERE, "overlay.yaml"])) as f:
        overlay = context_from_file(f)
    with pytest.raises(UndefinedError):
        ctx['user']['staff']['login']
    ctx.update(overlay)
    assert ctx['user']['staff']['login'] == "aa@bb.cc"


def test_bind(ctx):
    assert ctx['url']['base_url'] == "http://example.com"
    ctx.bind("url.base_url", "https://foo.bar")
    assert ctx['url']['base_url'] == "https://foo.bar"
    ctx.bind("url", "https://foo.bar")
    assert ctx['url'] == "https://foo.bar"


def test_ref_list(ctx):
    assert ctx.render(ctx["a"]["ref_list"]) == ["one", "two"]





