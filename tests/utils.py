from json import dumps, loads


class Response:
    def __init__(self, status=200, text="", json=None):
        self.status = status
        self._text = text
        self._json = json if json else {}

    async def text(self):
        if not self._text and self._json:
            return dumps(self._json, indent=2)
        return self._text

    async def json(self):
        if not self._json and self._text:
            return loads(self._text)
        return self._json

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        return


class MockHTTPClient:
    def __init__(self, status=200, text="", json=None):
        self.response = Response(status=status, text=text, json=json)

    def execute(self, *args, **kwargs):
        return self.response

    def get(self, *args, **kwargs):
        return self.execute(*args, **kwargs)

    def post(self, *args, **kwargs):
        return self.execute(*args, **kwargs)
