from io import StringIO
import os

import pytest

from santa.shell import TaskShell, TestShell
from santa.context import context_from_file

HERE = os.path.dirname(__file__)


def reset(buff):
    buff.seek(0)
    buff.truncate()


@pytest.fixture
def context():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    return ctx


class MockTask:
    def __init__(self, name=None):
        self.name = name

    def get_parts(self, ctx):
        return "/my/url/", dict(arg1=1), {"Content-Type": "application/json"}, {}


def test_taskshell(context):
    stdin = StringIO()
    stdout = StringIO()
    task = MockTask("mytask")
    shell = TaskShell(task, context, stdin=stdin, stdout=stdout)

    shell.print("foo")
    assert stdout.getvalue() == "foo\n"

    reset(stdout)
    shell.pprint("bar")
    assert stdout.getvalue() == "'bar'\n"

    reset(stdout)
    shell.print_yaml({"foo": 1})
    assert stdout.getvalue() == "{foo: 1}\n"

    assert shell.prompt == "mytask > "

    reset(stdout)
    shell.do_context("")
    assert stdout.getvalue() == "user\nurl\na\nc\nqs\n"

    reset(stdout)
    shell.do_context("url.query_string.")
    assert sorted(stdout.getvalue().strip().split("\n")) == ["a", "bar", "foo"]

    reset(stdout)
    shell.do_context("url.query_string")
    assert stdout.getvalue() == "url.query_string:\n  a: 1\n  bar: bar\n  foo: foo\n"

    shell.do_exit(None)
    assert shell.abort

    shell.do_reset(None)


def test_testshell(context):
    stdin = StringIO()
    stdout = StringIO()
    task = MockTask("mytask")
    shell = TestShell(task, context, stdin=stdin, stdout=stdout)
    shell.do_reset(None)

    reset(stdout)
    shell.do_url("")
    assert stdout.getvalue() == "/my/url/\n"

    reset(stdout)
    shell.do_qs("")
    assert stdout.getvalue() == repr(dict(arg1=1)) + "\n"

    reset(stdout)
    shell.do_headers("")
    assert stdout.getvalue() == "{'Content-Type': 'application/json'}\n"

    reset(stdout)
    shell.do_json_body("")
    assert stdout.getvalue() == "{}\n"

    reset(stdout)
    shell.do_jq("")
    assert stdout.getvalue() == "No response yet. Call 'run' first.\n"

    reset(stdout)
    shell.json = dict(stuff=1)
    shell.do_jq(".stuff")
    assert stdout.getvalue() == "1\n"
