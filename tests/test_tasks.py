from io import StringIO
import sys
import time

import pytest

from santa import init
from santa.tasks import Message, Sleep
from santa.tasks import Context as ContextTask
from santa.builtins.tasks import Test as TestTask
from santa.context import Context
from santa.task import TaskResult
from santa.parser import yamlloader_as_yaml

from .utils import MockHTTPClient


init()


@pytest.fixture
def context():
    data = """---
domain: example.com
content_type: application/json
foo: bar
fobar: baz
"""
    ctx = Context(data)
    ctx["_out"] = StringIO()
    return ctx


@pytest.mark.asyncio
async def test_message(context):
    m = Message(message="foo is {{ foo }}", context=["foo"])
    await m(context)
    expected_output = """
foo is bar
 foo:
  bar
  ...

"""
    out = context["_out"].getvalue()
    assert out == expected_output


@pytest.mark.asyncio
async def test_context(context):
    c = ContextTask(processors=[{"copy": {"src": "foo", "dest": "baz"}}])
    await c(context)
    assert context["baz"] == "bar"


def test_result(context):
    m = Message(message="foo is {{ foo }}", context=["foo"])
    r = TaskResult(m)

    # dict behaviour
    with pytest.raises(KeyError):
        r["foo.bar"]
    r["foo.bar.baz"] = 1
    assert r["foo.bar.baz"] == 1
    assert r["foo.bar"] == {"baz": 1}

    # dict export
    d = r.as_dict()
    assert d == {
        'duration': None,
        'errors': [],
        'result': {'foo': {'bar': {'baz': 1}}},
        'skipped': False,
        'success': True,
        'task_name': None,
        'type': 'message',
        'warnings': []
    }

    # logs
    with pytest.raises(ValueError,
                        match=r"Call set_context\(ctx\) before using it."):
        r.log_debug("hop")
    r.set_context(context)
    context["_debug"] = False
    r.log_debug("hop")
    assert context["_out"].getvalue() == ""
    context["_debug"] = True
    r.log_debug("hop")
    assert context["_out"].getvalue() == "hop\n"
    r.log_error("hip")
    assert r.errors == ["hip"]
    r.log_warning("hip")
    assert r.warnings == ["hip"]

    # exception handling
    try:
        1/0
    except ZeroDivisionError as e:
        r.log_exception(e, sys.exc_info()[2])


@pytest.mark.asyncio
async def test_test(context):
    t = TestTask(
        method="GET",
        url={
            "scheme": "https",
            "host": "{{ domain }}",
            "port": 8000,
            "path": "{{ foo }}",
            "headers": {
                "Content-Type": "{{ content_type }}"
            }
        }
    )
    t.__ctx__ = context
    url, qs, headers, json_body = t.get_parts(context)
    assert url == "https://example.com:8000/bar"
    assert qs == []
    assert headers == {"Content-Type": "application/json"}
    assert json_body is None
    context["_http_session"] = MockHTTPClient(200)
    await t(context)


@pytest.mark.asyncio
async def test_sleep(context):
    t = Sleep(time=200)
    start = time.time()
    await t(context)
    assert time.time() - start > 0.2


def test_render_test(context):
    t = TestTask(
        method="GET",
        url={
            "scheme": "https",
            "host": "{{ domain }}",
            "port": 8000,
            "path": "{{ foo }}",
            "headers": {
                "Content-Type": "{{ content_type }}"
            }
        }
    )
    t.__ctx__ = context
    print(yamlloader_as_yaml(t, name="test", comments=True))

def test_scaffold_test():
    print(TestTask.scaffold(comments=True))

