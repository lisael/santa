import os
import sys

import pytest

from santa import init
from santa.extractor import Extractor, extractor_factory


def test_extractor_plugin():
    init()
    with pytest.raises(ValueError):
        extractor_factory({"plugin1": {}})
    here = os.path.dirname(__file__)
    sys.path.append(here)
    init()
