import pytest
from santa.parser import (YamlLoaderBase, YamlLoader, BooleanField,
                          StringField, yamlloader_as_yaml)


def test_dict():
    class Dict(YamlLoader):
        string = StringField()
        boolean = BooleanField()
        fact = StringField(default=lambda: "bar")

    assert isinstance(Dict.string, StringField)
    d = Dict(boolean=False, string="Foo")
    assert not d.boolean
    assert d.string == "Foo"
    assert d.fact == "bar"
    with pytest.raises(AttributeError):
        d.undef
    del d.string
    with pytest.raises(AttributeError):
        d.string


def test_scafold():
    class Loader(YamlLoader):
        string = StringField()
        boolean = BooleanField(help="a bool")

    loader = Loader(
        boolean=False,
        string="foo",
    )

    result = yamlloader_as_yaml(loader, comments=True, name="loader")
    print(result)
    expected = """
loader:
  string: 'foo'
  # a bool
  boolean: no
"""
    assert result == expected[1:]


def test_factory():
    class LoaderMeta(YamlLoaderBase):
        registry = {}
        type_name = "loader"

    class Loader(YamlLoader, metaclass=LoaderMeta):
        string = StringField(is_main=True)

    Loader._factory(dict(loader="foo"))
    with pytest.raises(ValueError):
        Loader._factory(dict(loader=["foo"]))

    class Loader2(YamlLoader, metaclass=LoaderMeta):
        string = StringField()

    with pytest.raises(ValueError):
        Loader2._factory(dict(loader2="foo"))
