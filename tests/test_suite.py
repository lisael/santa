import pytest
from io import StringIO

from santa.context import Context
from santa.suite import Suite
from santa.tasks import Message, Fork


@pytest.fixture
def context():
    data = """---
domain: example.com
content_type: application/json
foo: bar
fobar: baz
stuff:
    - item1
    - item2
"""
    ctx = Context(data)
    ctx["_out"] = StringIO()
    return ctx


@pytest.mark.asyncio
async def test_suite(context):
    tasks = [
        {"message": "hello"},
        {"message": {"message": "{{ item }}", "with_items": ["i1", "i2"]}},
    ]
    s = Suite(tasks)
    await s(context)
    out = context["_out"].getvalue()
    assert out == """
hello

Suite: : OK

i1

Suite: : OK

i2

Suite: : OK
"""
