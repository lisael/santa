# CLI

## Auto-documentation

Santa tries to auto document as many parts as possible, to be sure
we keep the doc in sync with the code.

The `scaffold` subcommand is handy to remember the yaml structure of
a task:

```sh
santa scaffold --with-help tasks test
```

```
# Test an HTTP request. Validators and extractors can be provided.
test:
  # HTTP method of the request
  method: GET
  # URL object to test
  url:
    scheme: UNDEFINED!
    host: UNDEFINED!
    path: /
    port: ''
    headers: []
    query_string: []
  # JSON data sent
  json_body: []
  # List of validators
  validate: []
  # List of extractors
  extract: []
  # Outut debug logs
  debug: no
  interactive: no
  pdb: no
```

Using a modern text editor, it's easy to add helper macros from the `scaffold`
command. Here's an example with vim:

```vim
function! g:SantaScaffoldTask()
    execute "normal V"
    let type = getline("'<")[getpos("'<")[2]-1:getpos("'>")[2]]
    execute ":'<,'>!santa scaffold tasks " . type
endfunction

nnoremap <leader>v <esc>:call SantaScaffoldTask()<cr>
```

Once sourced, just type: `test<esc><leader>v` on a new line to insert a `test`
skeleton.
